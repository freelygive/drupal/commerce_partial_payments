<?php

namespace Drupal\commerce_partial_payments;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;

/**
 * Interface for the order item tracking service.
 */
interface OrderItemTrackingInterface {

  /**
   * Get the amount tracked for a particular order item.
   *
   * @param int $item
   *   The order item ID.
   *
   * @return \Drupal\commerce_price\Price
   *   The amount tracked.
   *
   * @throws \Exception
   *   Thrown if a the order item has payments in multiple currencies.
   * @throws \Exception
   *   Thrown if a default currency is needed and none could be worked out.
   */
  public function getTrackedAmountsForItem(int $item) : Price;

  /**
   * Get the amount tracked for a multiple order items.
   *
   * @param int[] $items
   *   An array of order item IDs.
   *
   * @return \Drupal\commerce_price\Price[]
   *   The amounts tracked keyed by order item ID. All $items will be included,
   *   but may use a default currency.
   *
   * @throws \Exception
   *   Thrown if a single order item has payments in multiple currencies.
   * @throws \Exception
   *   Thrown if a default currency is needed and none could be worked out.
   */
  public function getTrackedAmountsForItems(array $items);

  /**
   * Get the amount tracked for all order items on an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\commerce_price\Price[]
   *   The amounts tracked keyed by order item ID. All items on $order will be
   *   included, but may use a default currency.
   *
   * @throws \Exception
   *   Thrown if a single order item has payments in multiple currencies.
   * @throws \Exception
   *   Thrown if a default currency is needed and none could be worked out.
   */
  public function getTrackedAmountsForOrder(OrderInterface $order);

  /**
   * Get the amount untracked for an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\commerce_price\Price
   *   The untracked amount for the order.
   *
   * @throws \Exception
   *   Thrown if a single order item has payments in multiple currencies.
   * @throws \Exception
   *   Thrown if a default currency is needed and none could be worked out.
   */
  public function getUntrackedAmountForOrder(OrderInterface $order);

  /**
   * Initialise the tracking information for a payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment being created.
   *
   * @throws \Exception
   *   Thrown if the payment is not paying the full order balance, but there is
   *   no tracking information.
   */
  public function onPaymentCreate(PaymentInterface $payment) : void;

  /**
   * Calculate the default tracking information for an order.
   *
   * This would be the tracking information required to pay the order balance.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to pay.
   * @param bool $store
   *   Whether to store the tracking information in the tempstore.
   *
   * @return array
   *   An array of the tracking information. This should match the field value
   *   array for a commerce_tracked_amount field referencing order items.
   */
  public function calculatePaidInFullTracking(OrderInterface $order, bool $store = TRUE) : array;

  /**
   * Store the tracking information for an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   * @param array $tracking
   *   An array of the tracking information. This should match the field value
   *   array for a commerce_tracked_amount field referencing order items.
   */
  public function storeTrackingInformation(OrderInterface $order, array $tracking) : void;

  /**
   * Retrieve the tracking information for an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return array|null
   *   An array of the tracking information, or NULL if there isn't any. This
   *   should match the field value array for a commerce_tracked_amount field
   *   referencing order items.
   */
  public function loadTrackingInformation(OrderInterface $order) : ?array;

  /**
   * Validate the the tracking total matches the payment amount.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment to check.
   * @param array|null $tracking
   *   The tracking information, or NULL to pull from the payment.
   *
   * @return bool|null
   *   TRUE if the tracking is valid, FALSE if it's not, NULL if there is no
   *   tracking.
   */
  public function validateTracking(PaymentInterface $payment, ?array $tracking = NULL) : ?bool;

}
