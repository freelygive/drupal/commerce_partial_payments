<?php

namespace Drupal\commerce_partial_payments\Plugin\Commerce\InlineForm;

use Drupal\commerce_partial_payments\Element\TrackedAmounts;
use Drupal\commerce_partial_payments\Form\TrackingElementTrait;
use Drupal\commerce_partial_payments\OrderItemTrackingInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\InlineForm\PaymentGatewayForm;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extension of the payment gateway form to support payment tracking.
 */
class PartialPaymentsGatewayForm extends PaymentGatewayForm implements TrustedCallbackInterface {

  use TrackingElementTrait;

  /**
   * The order item payment tracker.
   *
   * @var \Drupal\commerce_partial_payments\OrderItemTrackingInterface
   */
  protected $paymentTracking;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $class = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $class->setStringTranslation($container->get('string_translation'));
    $class->setPaymentTracking($container->get('commerce_partial_payments.order_item_tracking'));
    $class->setCurrencyFormatter($container->get('commerce_price.currency_formatter'));
    $class->setElementInfoManager($container->get('plugin.manager.element_info'));
    $class->setCurrentLocale($container->get('commerce.current_locale'));
    $class->setMessenger($container->get('messenger'));
    return $class;
  }

  /**
   * Set the order item payment tracker.
   *
   * @param \Drupal\commerce_partial_payments\OrderItemTrackingInterface $paymentTracking
   *   The order item payment tracker.
   *
   * @return $this
   */
  public function setPaymentTracking(OrderItemTrackingInterface $paymentTracking) {
    $this->paymentTracking = $paymentTracking;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    // If this is a form for a method rather than a payment, we don't want to
    // make any modifications. For example, this happens in the payment
    // information checkout pane for onsite payment methods.
    if (!$this->entity instanceof PaymentInterface) {
      return parent::buildInlineForm($inline_form, $form_state);
    }

    // If the payment is new, doesn't have an explicitly zero amount and has no
    // tracking info, add the defaults.
    if ($this->entity->isNew() && $this->entity->get('order_item_tracking')->isEmpty()) {
      if ($this->entity->getAmount() === NULL) {
        $this->entity->set('order_item_tracking', $this->paymentTracking->calculatePaidInFullTracking($this->entity->getOrder(), FALSE));
      }
    }

    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    // If the order item tracking has been added by the gateway form, we wont
    // make any changes to it.
    if (isset($inline_form['order_item_tracking'])) {
      return $inline_form;
    }

    // Depending on the plugin form, we want to make some alterations.
    switch ($this->configuration['operation']) {
      case 'add-payment':
        $this->alterAddForm($inline_form, $form_state);
        break;

      case 'receive-payment':
        $this->alterReceiveForm($inline_form, $form_state);
        break;

      case 'refund-payment':
        $this->alterRefundForm($inline_form, $form_state);
        break;
    }

    return $inline_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateInlineForm(array &$inline_form, FormStateInterface $form_state) {
    if (isset($inline_form['#cpp_validate'])) {
      foreach ($inline_form['#cpp_validate'] as $callback) {
        // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
        call_user_func_array($this->prepareCallback($callback), [&$inline_form, &$form_state]);
      }
    }

    parent::validateInlineForm($inline_form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::submitInlineForm($inline_form, $form_state);

    if (isset($inline_form['#cpp_submit'])) {
      foreach ($inline_form['#cpp_submit'] as $callback) {
        // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
        call_user_func_array($this->prepareCallback($callback), [&$inline_form, &$form_state]);
      }
    }
  }

  /**
   * Convert ::callback into callables.
   *
   * @param array|string $callback
   *   The callback.
   *
   * @return array|string
   *   The prepared callback.
   */
  protected function prepareCallback($callback) {
    if (is_string($callback) && substr($callback, 0, 2) == '::') {
      $callback = [$this, substr($callback, 2)];
    }
    return $callback;
  }

  /**
   * Alter the add payment form.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function alterAddForm(array &$inline_form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    $order_balance = $order->getBalance();

    $this->swapAmountForAllocationChoice($inline_form, $this->t('pay'));

    // Add the pay in full option if there is a balance on the order.
    if ($order_balance) {
      // Only give the option if the amount is positive.
      if ($order_balance->isPositive()) {
        $full_label = $this->t('Pay the full balance of @balance', [
          '@balance' => $this->currencyFormatter->format($order_balance->getNumber(), $order_balance->getCurrencyCode()),
        ]);
        self::addAllocationOption($inline_form['partial_payments_type'], 'full', $full_label, $order_balance, $payment->get('order_item_tracking')->getValue(), -50);
        $inline_form['partial_payments_type']['#default_value'] = 'full';
      }
      // Otherwise, show a warning that there is no balance to pay.
      else {
        $this->messenger()->addWarning($this->t('This order is already paid in full.'));
      }
    }
    // If this order has no total, warn that it's a free order.
    else {
      $this->messenger()->addWarning($this->t('There is is of zero value.'));
    }

    // Add the specific tracking.
    $inline_form['order_item_tracking'] = [
      '#type' => 'commerce_partial_payments_tracked_amount',
      '#tracking' => $this->paymentTracking->getTrackedAmountsForOrder($order),
      '#default_value' => $payment->get('order_item_tracking')->getValue(),
      '#items' => [],
      '#empty' => $this->t('This order has no items.'),
      '#states' => [
        'visible' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
        'required' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
      ],
      '#weight' => 50,
    ];
    foreach ($order->getItems() as $item) {
      $inline_form['order_item_tracking']['#items'][$item->id()] = [
        'label' => $item->label(),
        'price' => $item->getAdjustedTotalPrice(),
      ];
    }
    // Zero any negative default values.
    foreach ($inline_form['order_item_tracking']['#default_value'] as &$default_value) {
      $amount = Price::fromArray($default_value);
      if ($amount->isNegative()) {
        $default_value['number'] = '0';
      }
    }

    // Set the order balance so we can dynamically show a warning.
    self::addTrackingJs($inline_form, [
      'warning' => $order_balance ? $order_balance->getNumber() : 0,
    ]);

    // Add the warning element, hidden by default.
    $inline_form['amount_warning'] = [
      '#theme_wrappers' => ['container'],
      '#weight' => 90,
      '#theme' => 'status_messages',
      '#message_list' => [
        'warning' => [
          $this->t('You are about to pay more than the balance of the order.'),
        ],
      ],
      '#attributes' => [
        'class' => [
          'cpp-tracking-warning',
          'hidden',
        ],
      ],
    ];

    $inline_form['#cpp_validate'][] = '::buildPaymentTracking';
  }

  /**
   * Alter the receive payment form.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function alterReceiveForm(array &$inline_form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $payment_amount = $payment->getAmount();
    $order = $payment->getOrder();
    $order_balance = $order->getBalance();

    $this->swapAmountForAllocationChoice($inline_form, $this->t('receive'));

    if (!$payment->get('order_item_tracking')->isEmpty()) {
      $full_label = $this->t('Receive the full @amount', [
        '@amount' => $this->currencyFormatter->format($payment_amount->getNumber(), $payment_amount->getCurrencyCode()),
      ]);
      self::addAllocationOption($inline_form['partial_payments_type'], 'full', $full_label, $payment_amount, $payment->get('order_item_tracking')->getValue(), -50);
      $inline_form['partial_payments_type']['#default_value'] = 'full';
    }

    // Add the specific tracking.
    $inline_form['order_item_tracking'] = [
      '#type' => 'commerce_partial_payments_tracked_amount',
      '#tracking' => $this->paymentTracking->getTrackedAmountsForOrder($order),
      '#default_value' => $payment->get('order_item_tracking')->getValue(),
      '#items' => [],
      '#empty' => $this->t('This order has no items.'),
      '#states' => [
        'visible' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
        'required' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
      ],
      '#weight' => 50,
    ];
    foreach ($order->getItems() as $item) {
      $inline_form['order_item_tracking']['#items'][$item->id()] = [
        'label' => $item->label(),
        'price' => $item->getAdjustedTotalPrice(),
      ];
    }

    // Set the order balance so we can dynamically show a warning.
    self::addTrackingJs($inline_form, [
      'warning' => $order_balance ? $order_balance->getNumber() : 0,
    ]);

    // Add the warning element, hidden by default.
    $inline_form['amount_warning'] = [
      '#theme_wrappers' => ['container'],
      '#weight' => 90,
      '#theme' => 'status_messages',
      '#message_list' => [
        'warning' => [
          $this->t('You are about to pay more than the balance of the order.'),
        ],
      ],
      '#attributes' => [
        'class' => [
          'cpp-tracking-warning',
          'hidden',
        ],
      ],
    ];

    $inline_form['#cpp_validate'][] = '::buildPaymentTracking';
  }

  /**
   * Alter the refund payment form.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function alterRefundForm(array &$inline_form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    // If this payment wasn't initially tracked, we won't make any adjustments.
    if ($payment->get('order_item_tracking')->isEmpty()) {
      return;
    }

    $payment_amount = $payment->getBalance();
    $order = $payment->getOrder();

    $this->swapAmountForAllocationChoice($inline_form, $this->t('refund'));

    $params = [
      '@amount' => $this->currencyFormatter->format($payment_amount->getNumber(), $payment_amount->getCurrencyCode()),
    ];
    $label = $payment->getRefundedAmount()->isPositive() ?
      $this->t('Refund the remaining @amount', $params) :
      $this->t('Refund the full @amount', $params);
    self::addAllocationOption($inline_form['partial_payments_type'], 'full', $label, $payment_amount, $payment->get('order_item_tracking')->getValue(), -50);
    $inline_form['partial_payments_type']['#default_value'] = 'full';

    // Add the specific tracking.
    $inline_form['order_item_tracking'] = [
      '#type' => 'commerce_partial_payments_tracked_amount',
      '#tracking_payment' => $payment->get('order_item_tracking')->getTrackedAmounts(),
      '#default_value' => $payment->get('order_item_tracking')->getValue(),
      '#items' => [],
      '#empty' => $this->t('This order has no items.'),
      '#header' => [
        'label' => '',
        'price' => $this->t('Price'),
        'tracked' => $this->t('Tracked'),
        'number' => $this->t('Refund'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
        'required' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
      ],
      '#weight' => 50,
    ];
    foreach ($order->getItems() as $item) {
      $inline_form['order_item_tracking']['#items'][$item->id()] = [
        'label' => $item->label(),
        'price' => $item->getAdjustedTotalPrice(),
      ];
    }

    // Set the order balance so we can dynamically show a warning.
    $order_paid = $order->getTotalPaid();
    $warn_amount = $order_paid && $order_paid->lessThan($payment_amount) ? $order_paid : $payment_amount;

    // Add the warning element, hidden by default.
    $inline_form['amount_warning'] = [
      '#theme_wrappers' => ['container'],
      '#weight' => 90,
      '#theme' => 'status_messages',
      '#attributes' => [
        'class' => [
          'cpp-tracking-warning',
        ],
      ],
    ];
    $params = [
      '@amount' => $this->currencyFormatter->format($warn_amount->getNumber(), $warn_amount->getCurrencyCode()),
    ];

    if ($warn_amount->isPositive()) {
      self::addTrackingJs($inline_form, [
        'warning' => $warn_amount->getNumber(),
      ]);
      $inline_form['amount_warning']['#attributes']['class'][] = 'hidden';
      $inline_form['amount_warning']['#message_list']['error'][] = $this->t('You cannot refund more than the original payment of @amount.', $params);
    }
    else {
      $inline_form['amount_warning']['#message_list']['error'][] = $this->t('You cannot refund more than the total paid towards this order of @amount.', $params);
    }

    $inline_form['#cpp_validate'][] = '::buildRefundTracking';
  }

  /**
   * Validation callback to build the tracking information for the payment.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function buildPaymentTracking(array &$inline_form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $type = $form_state->getValue($inline_form['partial_payments_type']['#parents']);

    // Get partial tracking values out of the form state.
    if ($type == 'partial') {
      $tracking_values = $form_state->getValue($inline_form['order_item_tracking']['#parents']);
      $tracking_values = TrackedAmounts::extractValues($tracking_values, $amount);
    }
    else {
      $amount = $inline_form['partial_payments_type'][$type]['#amount'];
      $tracking_values = $inline_form['partial_payments_type'][$type]['#tracking'];
    }

    $payment->set('order_item_tracking', $tracking_values);
    $payment->setAmount($amount);
    $form_state->setValue($inline_form['amount']['#parents'], $amount->toArray());
  }

  /**
   * Validation callback to build the tracking information for a refund.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function buildRefundTracking(array &$inline_form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $type = $form_state->getValue($inline_form['partial_payments_type']['#parents']);

    // Get partial tracking values out of the form state.
    if ($type == 'partial') {
      $tracking_values = $form_state->getValue($inline_form['order_item_tracking']['#parents']);
      $tracking_values = TrackedAmounts::extractValues($tracking_values, $amount, $payment->get('order_item_tracking')->getTrackedAmounts());
    }
    else {
      $amount = $inline_form['partial_payments_type'][$type]['#amount'];
      $tracking_values = $inline_form['partial_payments_type'][$type]['#tracking'];
    }

    // Check we're not refunding more than the payment amount.
    $payment_balance = $payment->getBalance();
    if (!$payment_balance || $payment_balance->lessThan($amount)) {
      $form_state->setError($inline_form, $this->t('You cannot refund more than the original payment.'));
      return;
    }

    // Check we're not refunding more than the order paid total.
    $order_paid = $payment->getOrder()->getTotalPaid();
    if (!$order_paid || $order_paid->lessThan($amount)) {
      $form_state->setError($inline_form, $this->t('You cannot refund more than the total paid towards the order.'));
      return;
    }

    $payment->set('order_item_tracking', $tracking_values);
    $form_state->setValue($inline_form['amount']['#parents'], $amount->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['formatAmount'];
  }

}
