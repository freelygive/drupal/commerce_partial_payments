<?php

namespace Drupal\commerce_partial_payments\Plugin\Field\FieldType;

use Drupal\commerce_price\Price;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'commerce_tracked_amount' field type.
 *
 * @property int target_id
 * @property string number
 * @property string currency_code
 *
 * @FieldType(
 *   id = "commerce_tracked_amount",
 *   label = @Translation("Tracked amount"),
 *   category = @Translation("Commerce"),
 *   list_class = "\Drupal\commerce_partial_payments\Plugin\Field\CommerceTrackedAmountItemList",
 *   no_ui = true,
 * )
 */
class CommerceTrackedAmountItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'commerce_order_item',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // No number means the field is empty ('0' is allowed).
    return $this->number === NULL || $this->number === '' ||
      // No currency code means the field is empty.
      empty($this->currency_code) ||
      // Parent will check the target ID is set and exists.
      parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['number'] = DataDefinition::create('string')
      ->setLabel(t('Number'))
      ->setRequired(FALSE);

    $properties['currency_code'] = DataDefinition::create('string')
      ->setLabel(t('Currency code'))
      ->setRequired(FALSE);

    $properties['formatted'] = DataDefinition::create('formatted_price')
      ->setLabel(t('Formatted price'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints[] = $manager->create('ComplexData', [
      'number' => [
        'Regex' => [
          'pattern' => '/^[+-]?((\d+(\.\d*)?)|(\.\d+))$/i',
        ],
      ],
    ]);
    $constraints[] = $manager->create('TrackedAmounts', []);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['number'] = [
      'description' => 'The number.',
      'type' => 'numeric',
      'precision' => 19,
      'scale' => 6,
    ];
    $schema['columns']['currency_code'] = [
      'description' => 'The currency code.',
      'type' => 'varchar',
      'length' => 3,
    ];

    $schema['indexes']['currency_code'] = ['currency_code'];

    return $schema;
  }

  /**
   * Get the amount and currency as a price object.
   *
   * @return \Drupal\commerce_price\Price
   *   The price object.
   */
  public function toPrice() {
    return new Price($this->number, $this->currency_code);
  }

}
