<?php

namespace Drupal\commerce_partial_payments\Plugin\Field;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed field class for the order item tracking.
 *
 * @package Drupal\commerce_partial_payments\Plugin\Field
 */
class ComputedOrderItemTracking extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\commerce_order\Entity\OrderItem $entity */
    $entity = $this->getEntity();

    // If this is not new and there is an order, look up the existing amounts.
    if (!$entity->isNew() && $order = $entity->getOrder()) {
      /** @var \Drupal\commerce_partial_payments\OrderItemTrackingInterface $tracker */
      $tracker = \Drupal::service('commerce_partial_payments.order_item_tracking');
      $paid = $tracker->getTrackedAmountsForOrder($order);

      if (!isset($paid[$entity->id()])) {
        return;
      }

      $amount = $paid[$entity->id()];
    }
    // Otherwise the tracked amount will always be zero. Use the order item
    // price for the currency.
    elseif ($amount = $entity->getTotalPrice()) {
      $amount = $amount->multiply(0);
    }
    // Otherwise we have no currency, so have no items.
    else {
      return;
    }

    // If we're actually after the balance, subtract the paid from the total.
    if ($this->getSetting('tracking_balance') ?: FALSE) {
      $price = $entity->getAdjustedTotalPrice();
      $amount = $amount ? $price->subtract($amount) : $price;
    }

    // Add the item.
    $this->list[0] = $this->createItem(0, $amount);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultAccess($operation = 'view', ?AccountInterface $account = NULL) {
    if (!isset($account)) {
      $account = \Drupal::currentUser();
    }

    // Always allow for the administer payments permission.
    $result = AccessResult::allowedIfHasPermission($account, 'administer commerce_payment');
    if ($result->isAllowed()) {
      return $result;
    }

    // Otherwise, check depending on the settings.
    $config = \Drupal::config('commerce_partial_payments.settings');
    $tracking_access = $config->get('tracking_access') ?? [];
    $result->addCacheableDependency($config);

    // If there are no settings, we can return now.
    if (empty($tracking_access)) {
      return $result;
    }

    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $this->getEntity();
    $result->addCacheableDependency($order_item);

    $order = $order_item->getOrder();
    if ($order) {
      $result->addCacheableDependency($order);

      // Check the operations on the order.
      foreach ($tracking_access as $operation) {
        $result = $result->orIf($order->access($operation, $account, TRUE));
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    return [];
  }

  /**
   * Override to always recompute the value.
   *
   * Order Item Tracking values can vary based on price changes (including
   * discounts) or on payment info changes. Therefore, caching the value can
   * lead to out of date values persisting during a request.
   */
  public function ensureComputedValue() {
    $this->computeValue();
  }

}
