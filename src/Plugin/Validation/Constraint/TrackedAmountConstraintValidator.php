<?php

namespace Drupal\commerce_partial_payments\Plugin\Validation\Constraint;

use Drupal\commerce_partial_payments\OrderItemTrackingInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Bookkeeping Entries constraint.
 */
class TrackedAmountConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The order item tracking service.
   *
   * @var \Drupal\commerce_partial_payments\OrderItemTrackingInterface
   */
  protected $tracker;

  /**
   * Construct the validator.
   */
  public function __construct(OrderItemTrackingInterface $tracker) {
    $this->tracker = $tracker;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('commerce_partial_payments.order_item_tracking'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    /** @var \Drupal\commerce_partial_payments\Plugin\Validation\Constraint\TrackedAmountConstraint $constraint */
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    // Don't throw errors when there is no tracking.
    if ($items->isEmpty()) {
      return;
    }

    $payment = $items->getEntity();
    if (!$this->tracker->validateTracking($payment)) {
      $this->context->buildViolation($constraint->error)
        ->addViolation();
    }
  }

}
