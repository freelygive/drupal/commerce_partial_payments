<?php

namespace Drupal\commerce_partial_payments\Form;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce\CurrentLocaleInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Trait to help build forms with order item tracking.
 */
trait TrackingElementTrait {

  use StringTranslationTrait;

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * The element info manager.
   *
   * @var \Drupal\Core\Render\ElementInfoManagerInterface
   */
  protected $elementInfoManager;

  /**
   * The current locale.
   *
   * @var \Drupal\commerce\CurrentLocaleInterface
   */
  protected $currentLocale;

  /**
   * Set the currency formatter.
   *
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   The currency formatter.
   *
   * @return $this
   */
  public function setCurrencyFormatter(CurrencyFormatterInterface $currency_formatter) {
    $this->currencyFormatter = $currency_formatter;
    return $this;
  }

  /**
   * Set the element info manager.
   *
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $element_info_manager
   *   The element info manager.
   *
   * @return $this
   */
  public function setElementInfoManager(ElementInfoManagerInterface $element_info_manager) {
    $this->elementInfoManager = $element_info_manager;
    return $this;
  }

  /**
   * Set the current locale service.
   *
   * @param \Drupal\commerce\CurrentLocaleInterface $currentLocale
   *   The current locale service.
   *
   * @return $this
   */
  public function setCurrentLocale(CurrentLocaleInterface $currentLocale) {
    $this->currentLocale = $currentLocale;
    return $this;
  }

  /**
   * Swap the amount for an allocation choice.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $operation
   *   The operation being performed.
   */
  protected function swapAmountForAllocationChoice(array &$inline_form, TranslatableMarkup $operation) {
    $params = [
      '@operation' => $operation,
    ];

    // Add the allocation choice.
    $inline_form['partial_payments_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('What would you like to @operation?', $params),
      '#options' => [
        'partial' => $this->t('Choose how much to @operation and how it is allocated.', $params),
      ],
      '#default_value' => 'partial',
      '#weight' => -1,
    ];

    // Swap the amount from an input to an item.
    $inline_form['amount']['#type'] = 'item';
    $inline_form['amount']['#weight'] = 80;
    $inline_form['amount']['#title'] = $this->t('Total to @operation', $params);
    $inline_form['amount']['#required'] = FALSE;
    $inline_form['amount']['#tracking_amount'] = TRUE;

    // Only show if we have selected partials, as other options will specify
    // the amount.
    $inline_form['amount']['#states'] = [
      'visible' => [
        ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
      ],
    ];

    // Add a pre-render to format the amount based on it's current value.
    $inline_form['amount']['#pre_render'] = $this->elementInfoManager->getInfoProperty('item', '#pre_render', []);
    $inline_form['amount']['#pre_render'][] = [$this, 'formatAmount'];

    // Add the library to update the amount dynamically.
    self::addTrackingJs($inline_form, [
      'locale' => $this->currentLocale->getLocale()->getLocaleCode(),
      'currency' => $inline_form['amount']['#default_value']['currency_code'],
    ]);
  }

  /**
   * Add an allocation option.
   *
   * @param array $element
   *   The payment type selection element array.
   * @param string $key
   *   The key for the option.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The label for the option.
   * @param \Drupal\commerce_price\Price $amount
   *   The payment amount.
   * @param array $tracking
   *   The order item tracking values.
   * @param int|null $weight
   *   The weight for the option or NULL to not assign one.
   */
  public static function addAllocationOption(array &$element, string $key, TranslatableMarkup $label, Price $amount, array $tracking, int|null $weight = NULL) {
    $element['#options'][$key] = $label;
    if ($weight) {
      $element[$key]['#weight'] = $weight;
    }
    $element[$key]['#amount'] = $amount;
    $element[$key]['#tracking'] = $tracking;
  }

  /**
   * Add the tracking javascript, including settings.
   *
   * @param array $form
   *   The form containing the order items and total amount.
   * @param array $settings
   *   The settings.
   */
  public static function addTrackingJs(array &$form, array $settings) {
    // Merge our settings into any existing values.
    $parents = [
      '#attached',
      'drupalSettings',
      'commerce_partial_payments',
      'forms',
      '#' . $form['#id'],
    ];
    $settings = $settings + (NestedArray::getValue($form, $parents) ?? []);
    NestedArray::setValue($form, $parents, $settings);

    // Add the library if we don't already have it.
    if (!isset($form['#attached']['library']) || !in_array('commerce_partial_payments/tracking_total', $form['#attached']['library'])) {
      $form['#attached']['library'][] = 'commerce_partial_payments/tracking_total';
    }
  }

  /**
   * Pre render callback to format the total amount.
   *
   * @param array $element
   *   The form element.
   *
   * @return array
   *   The form element.
   */
  public function formatAmount(array $element) {
    $formatted = $this->currencyFormatter->format($element['#value']['number'], $element['#value']['currency_code']);
    if (!empty($element['#tracking_amount'])) {
      $element['#markup'] = new FormattableMarkup('<span class="cpp-tracking-total">@amount</span>', [
        '@amount' => $formatted,
      ]);
    }
    else {
      $element['#markup'] = $formatted;
    }
    return $element;
  }

}
