<?php

namespace Drupal\commerce_partial_payments\Form;

use Drupal\commerce\CurrentLocaleInterface;
use Drupal\commerce_partial_payments\Element\TrackedAmounts;
use Drupal\commerce_partial_payments\OrderItemTrackingInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Commerce Partial Payments form.
 */
class AddTrackingForm extends FormBase implements TrustedCallbackInterface {

  use TrackingElementTrait;

  /**
   * The order item tracking service.
   *
   * @var \Drupal\commerce_partial_payments\OrderItemTrackingInterface
   */
  protected $paymentTracking;

  /**
   * The current locale.
   *
   * @var \Drupal\commerce\CurrentLocaleInterface
   */
  protected $currentLocale;

  /**
   * The payment being modified.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $class = new static(
      $container->get('commerce_partial_payments.order_item_tracking'),
      $container->get('commerce.current_locale')
    );
    $class->setCurrencyFormatter($container->get('commerce_price.currency_formatter'));
    return $class;
  }

  /**
   * Construct the AddTrackingForm.
   *
   * @param \Drupal\commerce_partial_payments\OrderItemTrackingInterface $tracking
   *   The order item tracking service.
   * @param \Drupal\commerce\CurrentLocaleInterface $current_locale
   *   The current locale service.
   */
  public function __construct(OrderItemTrackingInterface $tracking, CurrentLocaleInterface $current_locale) {
    $this->paymentTracking = $tracking;
    $this->currentLocale = $current_locale;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_partial_payments_add_tracking';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?PaymentInterface $payment = NULL) {
    if (!$payment) {
      throw new \InvalidArgumentException('AddTrackingForm requires a payment.');
    }
    $this->payment = $payment;
    $form['#payment_gateway_id'] = $payment->getPaymentGatewayId();

    $order = $payment->getOrder();
    $order_amount = $order->getTotalPrice();
    $payment_amount = $payment->getAmount();
    $default_amount = $payment_amount;

    // If the payment is for the full order amount, we should be able to use
    // the calculated tracking amounts.
    if ($order_amount && $payment_amount->equals($order_amount)) {
      $default_tracking = $this->paymentTracking->calculatePaidInFullTracking($order);
    }
    // Otherwise leave it empty.
    else {
      $default_tracking = [];
      $default_amount = $default_amount->multiply('0');
    }

    $form['payment_amount'] = [
      '#type' => 'item',
      '#title' => $this->t('Payment amount'),
      '#value' => $payment_amount->toArray(),
      '#pre_render' => [[$this, 'formatAmount']],
    ];

    // Pull the current amount from user input, if available.
    $user_input = $form_state->getUserInput();
    if (isset($user_input['order_item_tracking'])) {
      TrackedAmounts::extractValues($user_input['order_item_tracking'], $current_total);
    }
    else {
      $current_total = $default_amount;
    }

    $form['amount'] = [
      '#type' => 'item',
      '#title' => $this->t('Tracked amount'),
      '#value' => $current_total->toArray(),
      '#pre_render' => [[$this, 'formatAmount']],
      '#weight' => 51,
      '#tracking_amount' => TRUE,
    ];

    $form['order_item_tracking'] = [
      '#type' => 'commerce_partial_payments_tracked_amount',
      '#tracking' => $this->paymentTracking->getTrackedAmountsForOrder($order),
      '#default_value' => $default_tracking,
      '#items' => [],
      '#empty' => $this->t('This order has no items.'),
      '#states' => [
        'visible' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
        'required' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
      ],
      '#weight' => 50,
    ];
    foreach ($order->getItems() as $item) {
      $form['order_item_tracking']['#items'][$item->id()] = [
        'label' => $item->label(),
        'price' => $item->getAdjustedTotalPrice(),
      ];
    }

    // Add the warning element, hidden by default.
    $form['amount_warning'] = [
      '#theme_wrappers' => ['container'],
      '#weight' => 90,
      '#theme' => 'status_messages',
      '#message_list' => [
        'error' => [
          $this->t('You must track the exactly %amount.', [
            '%amount' => $this->currencyFormatter->format($payment_amount->getNumber(), $payment_amount->getCurrencyCode()),
          ]),
        ],
      ],
      '#attributes' => [
        'class' => [
          'cpp-tracking-warning',
          'hidden',
        ],
      ],
    ];

    $form['#process'][] = [$this, 'processForm'];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add tracking'),
    ];

    return $form;
  }

  /**
   * Process callback to add the tracking javascript.
   */
  public function processForm(&$form, FormStateInterface $form_state) {
    self::addTrackingJs($form, [
      'locale' => $this->currentLocale->getLocale()->getLocaleCode(),
      'currency' => $form['payment_amount']['#value']['currency_code'],
      'warning' => $form['payment_amount']['#value']['number'],
      'warning_type' => 'exact',
    ]);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Get the tracking values out of the form state.
    $tracking_values = $form_state->getValue('order_item_tracking');
    $tracking_values = TrackedAmounts::extractValues($tracking_values, $total);
    $payment_amount = $this->payment->getAmount();

    if ($payment_amount->equals($total)) {
      $this->payment->set('order_item_tracking', $tracking_values);
    }
    else {
      $form_state->setError($form['order_item_tracking'], $this->t('You must track the exactly %amount.', [
        '%amount' => $this->currencyFormatter->format($payment_amount->getNumber(), $payment_amount->getCurrencyCode()),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->payment->save();
    $this->messenger()->addStatus('Payment tracking saved.');
    $form_state->setRedirect('entity.commerce_payment.collection', [
      'commerce_order' => $this->payment->getOrder()->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['formatAmount'];
  }

}
