<?php

namespace Drupal\commerce_partial_payments\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Commerce Partial Payments settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected PermissionHandlerInterface $permissionHandler;

  /**
   * Construct the form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, PermissionHandlerInterface $permission_handler) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->permissionHandler = $permission_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('user.permissions')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_partial_payments_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_partial_payments.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['tracking_access'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('What permission is required to view payment tracking?'),
      '#default_value' => $this->config('commerce_partial_payments.settings')->get('tracking_access') ?? [],
      '#options' => [
        'view' => $this->t('View the order'),
        'update' => $this->t('Update the order'),
      ],
    ];

    $payment_permission = $this->permissionHandler->getPermissions()['administer commerce_payment'] ?? NULL;
    if ($payment_permission) {
      $form['tracking_access']['#description'] = $this->t('Anyone with the %permission permission will have access regardless of this setting.', [
        '%permission' => $payment_permission['title'],
      ]);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_partial_payments.settings')
      ->set('tracking_access', array_filter($form_state->getValue('tracking_access')))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
