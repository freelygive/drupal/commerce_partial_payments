<?php

namespace Drupal\commerce_partial_payments\Element;

use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Table;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a form element for tracked payment allocation.
 *
 * Properties include all properties of the table element plus:
 * - #tracking: Optionally provide full order's tracked values.
 * - #tracking_payment: Optionally provide this payment's pending values.
 * - #default_value: The default value for the tracking, keyed by target id.
 *   Can be omitted.
 * - #header: The headers to show. Omit to use the defaults. The 'number' column
 *   is required and will contain the price form element.
 * - #items: Additional details for the rows. Valid keys are the keys of the
 *   headers. By default, these are:
 *   - label: The label to show for the row.
 *   - price: The total price for the row. This is required for automatic
 *     calculation of the balance.
 *   - balance: The outstanding balance for the row. If omitted it will be
 *     calculated from #tracking and price.
 *   In addition to the defaults, the following can be added as headers:
 *   - tracked: The amount tracked for this item in this payment. If omitted it
 *     will be calculated from #tracking_payment.
 *
 * It is no compatible with the following standard table elements:
 * - #table_select
 * - #tabledrag
 *
 * Usage Example:
 * @code
 * $tracking = \Drupal::service('commerce_partial_payments.order_item_tracking');
 * $form['order_item_tracking'] = [
 *   '#type' => 'commerce_partial_payments_tracked_amount',
 *   '#tracking' => $tracking->getTrackedAmountsForOrder($order),
 *   '#default_value' => $tracking->calculatePaidInFullTracking($order, FALSE),
 *   '#items' => [],
 * ];
 * foreach ($order->getItems() as $item) {
 *   $form['order_item_tracking']['#items'][$item->id()] = [
 *     'label' => $item->label(),
 *     'price' => $item->getAdjustedTotal(),
 *   ];
 * }
 * @endcode
 *
 * @RenderElement("commerce_partial_payments_tracked_amount")
 */
class TrackedAmounts extends Table {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info = [
      '#tracking' => NULL,
      '#tracking_payment' => NULL,
      '#limit_to_tracked' => FALSE,
      '#header' => NULL,
      '#default_value' => [],
      '#items' => [],
      '#allow_negative' => FALSE,
      '#theme_wrappers' => ['form_element'],
      '#wrapper_attributes' => [
        'class' => ['cpp-tracking-amounts'],
      ],
    ] + $info;
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public static function processTable(&$element, FormStateInterface $form_state, &$complete_form) {
    $formatter = \Drupal::service('commerce_price.currency_formatter');

    // Get our default headers if not provided.
    if (!isset($element['#header'])) {
      $element['#header'] = [
        'label' => '',
        'price' => new TranslatableMarkup('Price'),
        'balance' => new TranslatableMarkup('Balance'),
        'number' => new TranslatableMarkup('Payment'),
      ];
    }
    if (!isset($element['#header']['number'])) {
      throw new \Exception('The number column is required for the tracked amounts element.');
    }

    $mapped_default = self::getMappedValues($element['#default_value']);
    $price_processors = \Drupal::service('plugin.manager.element_info')->getInfoProperty('commerce_price', '#process', []);
    $price_processors[] = [static::class, 'processPrice'];

    // Add each of our items.
    foreach ($element['#items'] as $target_id => $item_values) {
      foreach (array_keys($element['#header']) as $column) {
        if ($column == 'number') {
          if (isset($mapped_default[$target_id])) {
            $item_default = $mapped_default[$target_id];
          }
          elseif (isset($item_values['price']) && $item_values['price'] instanceof Price) {
            $item_default = $item_values['price']->multiply(0);
          }
          else {
            $item_default = NULL;
          }

          $value = $item_default instanceof Price ? $item_default->toArray() : $item_default;

          if (!empty($item_values['number:hide'])) {
            $element[$target_id][$column] = [
              '#type' => 'value',
              '#value' => $value,
            ];
          }
          else {
            $element[$target_id][$column] = [
              '#type' => 'commerce_price',
              '#title' => new TranslatableMarkup('Amount'),
              '#title_display' => 'invisible',
              '#default_value' => $value,
              '#value_callback' => [],
              '#process' => $price_processors,
              '#allow_negative' => $element['#allow_negative'] ?? FALSE,
            ];

            if (isset($item_values['number:min'])) {
              $element[$target_id][$column]['#min'] = $item_values['number:min'];
            }
            if (isset($item_values['number:max'])) {
              $element[$target_id][$column]['#max'] = $item_values['number:max'];
            }
          }
        }
        elseif (isset($item_values[$column])) {
          $cell = $item_values[$column];
          if (is_array($cell)) {
            $element[$target_id][$column] = $cell;
          }
          else {
            if ($cell instanceof Price) {
              $cell = $formatter->format($item_values[$column]->getNumber(), $item_values[$column]->getCurrencyCode());
            }
            $element[$target_id][$column] = ['#markup' => $cell];
          }
        }
        elseif ($column == 'balance' && isset($item_values['price']) && isset($element['#tracking'])) {
          /** @var \Drupal\commerce_price\Price $balance */
          $balance = $item_values['price'];
          if (isset($element['#tracking'][$target_id])) {
            $balance = $balance->subtract($element['#tracking'][$target_id]);
          }
          // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
          $element[$target_id][$column] = ['#markup' => $formatter->format($balance->getNumber(), $balance->getCurrencyCode())];
        }
        elseif ($column == 'tracked') {
          if (isset($element['#tracking_payment'][$target_id])) {
            /** @var \Drupal\commerce_price\Price $paid */
            $paid = $element['#tracking_payment'][$target_id];
            // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
            $element[$target_id][$column] = ['#markup' => $formatter->format($paid->getNumber(), $paid->getCurrencyCode())];
          }
          elseif (isset($item_values['price']) && $item_values['price'] instanceof Price) {
            // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
            $element[$target_id][$column] = ['#markup' => $formatter->format(0, $item_values['price']->getCurrencyCode())];
          }
          else {
            $element[$target_id][$column] = ['#markup' => '0'];
          }
        }
        else {
          $element[$target_id][$column] = [];
        }
      }
    }

    return $element;
  }

  /**
   * Process callback to set the min/max price on the number element.
   *
   * @param array $element
   *   The price element.
   *
   * @return array
   *   The price element.
   */
  public static function processPrice(array $element) {
    if (isset($element['#min'])) {
      $element['number']['#min'] = $element['#min'];
    }
    if (isset($element['#max'])) {
      $element['number']['#max'] = $element['#max'];
    }
    return $element;
  }

  /**
   * Get the values mapped by target id.
   *
   * @param array $values
   *   The field value.
   *
   * @return \Drupal\commerce_price\Price[]
   *   The amounts keyed by target id.
   */
  public static function getMappedValues(array $values) {
    $mapped = [];
    foreach ($values as $value) {
      $mapped[$value['target_id']] = Price::fromArray($value);
    }
    return $mapped;
  }

  /**
   * Extract the submitted values into the field items array.
   *
   * @param array $input
   *   The form values array.
   * @param \Drupal\commerce_price\Price|null $total
   *   By reference variable for tracking the total price.
   * @param array|null $refund_from
   *   Existing tracked amounts to refund from, if processing a refund. $total
   *   will contain the refund total.
   *
   * @return array
   *   The tracked amounts field items array.
   */
  public static function extractValues(array $input, ?Price &$total, array|null $refund_from = NULL) {
    // Convert the submitted values into field values.
    $values = [];
    foreach ($input as $target_id => $value) {
      $amount = new Price($value['number']['number'] ?: '0', $value['number']['currency_code']);

      // Track our total.
      $total = $total ? $total->add($amount) : $amount;

      // Make adjustments for a refund.
      if (isset($refund_from)) {
        // If there was an original tracked amount, subtract from it.
        if (isset($refund_from[$target_id])) {
          $amount = $refund_from[$target_id]->subtract($amount);
        }
        // Otherwise we post the negative of the refund amount.
        else {
          $amount = $amount->multiply('-1');
        }
      }

      // Skip any zero amounts.
      if ($amount->isZero()) {
        continue;
      }

      // Add our tracking to the values.
      $values[] = ['target_id' => $target_id] + $amount->toArray();
    }
    return $values;
  }

}
