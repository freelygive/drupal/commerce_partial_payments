<?php

namespace Drupal\commerce_partial_payments;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Psr\Log\LoggerInterface;

/**
 * Service for tracking order items.
 */
class OrderItemTracking implements OrderItemTrackingInterface {

  /**
   * The storage for the order item tracking.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $route;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The payment entity storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected $paymentStorage;

  /**
   * The default currency for the site.
   *
   * @var string
   */
  protected $defaultCurrency;

  /**
   * Constructs the Order Item Tracking service.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route
   *   The current route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The commerce partial payments logger channel.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, CurrentRouteMatch $route, EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger) {
    $this->privateTempStore = $temp_store_factory->get('commerce_partial_payments.order_item_tracking');
    $this->route = $route;
    $this->logger = $logger;

    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');

    $store = $entity_type_manager
      ->getStorage('commerce_store')
      ->loadDefault();
    if ($store) {
      $this->defaultCurrency = $store->getDefaultCurrencyCode();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackedAmountsForItem(int $item) : Price {
    // Query by the order item ID.
    $results = $this->getQuery()
      ->condition('order_item_tracking.target_id', $item)
      ->execute();
    $tracking = $this->extractResults($results, [$item]);
    return $tracking[$item];
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackedAmountsForItems(array $items) {
    // If $items is empty, there is nothing to do.
    if (empty($items)) {
      return [];
    }

    // Query by the order item IDs.
    $results = $this->getQuery()
      ->condition('order_item_tracking.target_id', $items)
      ->execute();
    return $this->extractResults($results, $items);
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackedAmountsForOrder(OrderInterface $order) {
    // Gather the item IDs for the defaults.
    $items = array_map(function (array $item) {
      return $item['target_id'];
    }, $order->get('order_items')->getValue());

    // Get a sensible default currency if possible.
    if ($total = $order->getTotalPrice()) {
      $default_currency = $total->getCurrencyCode();
    }
    else {
      $default_currency = $order->getStore()->getDefaultCurrencyCode();
    }

    // Query by payments belonging to a particular order.
    $results = $this->getQuery()
      ->condition('order_id', $order->id())
      ->execute();
    return $this->extractResults($results, $items, $default_currency);
  }

  /**
   * Get the aggregate query for tracked amounts.
   */
  protected function getQuery() : QueryAggregateInterface {
    return $this->paymentStorage->getAggregateQuery()
      ->accessCheck(TRUE)
      ->groupBy('order_item_tracking.target_id')
      ->groupBy('order_item_tracking.currency_code')
      ->aggregate('order_item_tracking.number', 'SUM')
      ->condition('state', 'refunded', '!=')
      ->exists('completed');
  }

  /**
   * Extract the tracking results from a query.
   *
   * @param array $results
   *   The results from the query.
   * @param array $items
   *   Optionally an array of item IDs to include with zero amounts if there is
   *   no explicit tracking information.
   * @param string|null $default_currency
   *   Optionally the currency to use for default items. If there are any
   *   tracking results, we will use that in preference. If not provided, we
   *   fall back to the default store's default currency, or throw an exception
   *   if there is none and we hit that scenario.
   *
   * @return \Drupal\commerce_price\Price[]
   *   An array of price objects keyed by order item id.
   *
   * @throws \Exception
   *   Thrown if a single order item has payments in multiple currencies.
   * @throws \Exception
   *   Thrown if a default currency is needed, none was provided and there isn't
   *   a default store to fall back on.
   */
  protected function extractResults(array $results, array $items = [], string|null $default_currency = NULL) {
    $tracking = [];

    // Extract results.
    foreach ($results as $result) {
      // Adding an exists condition to the query causes double joins and
      // therefore overcounting. So we'll handle them here instead.
      if (!isset($result['order_item_trackingnumber_sum'])) {
        continue;
      }

      // If we have multiple rows for the same order item, we must have more
      // than one currency. We don't currently support that.
      if (isset($tracking[$result['order_item_tracking_target_id']])) {
        throw new \Exception("All tracked amounts must be of the same currency for order item {$result['order_item_tracking_target_id']}.");
      }

      // Store the amount.
      $tracking[$result['order_item_tracking_target_id']] = new Price($result['order_item_trackingnumber_sum'], $result['order_item_tracking_currency_code']);

      // Override the default currency to be as accurate as possible.
      $default_currency = $result['order_item_tracking_currency_code'];
    }

    // Add in any items we don't have explicit tracking values for.
    foreach ($items as $item_id) {
      if (!isset($tracking[$item_id])) {
        $tracking[$item_id] = $this->getEmptyPrice($default_currency);
      }
    }

    return $tracking;
  }

  /**
   * {@inheritdoc}
   */
  public function getUntrackedAmountForOrder(OrderInterface $order) {
    $results = $this->paymentStorage->getAggregateQuery()
      ->accessCheck(TRUE)
      ->notExists('order_item_tracking.target_id')
      ->condition('order_id', $order->id())
      ->groupBy('amount.currency_code')
      ->aggregate('amount.number', 'SUM')
      ->condition('state', 'refunded', '!=')
      ->exists('completed')
      ->execute();

    // If we have a result, we can simply return it.
    if ($results) {
      // However, we don't support multiple currencies for a single order.
      if (count($results) > 1) {
        throw new \Exception("All tracked amounts must be of the same currency for order {$order->id()}.");
      }

      return new Price($results[0]['amountnumber_sum'], $results[0]['amount__currency_code']);
    }
    // Otherwise, attempt to build an empty price.
    else {
      // Get a sensible default currency if possible.
      if ($total = $order->getTotalPrice()) {
        $default_currency = $total->getCurrencyCode();
      }
      else {
        $default_currency = $order->getStore()->getDefaultCurrencyCode();
      }
      return $this->getEmptyPrice($default_currency);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onPaymentCreate(PaymentInterface $payment) : void {
    // Do nothing if we already have tracking information.
    if (!$payment->get('order_item_tracking')->isEmpty()) {
      return;
    }

    // Do nothing if the price is explicitly zero.
    $payment_amount = $payment->getAmount();
    if ($payment_amount && $payment_amount->isZero()) {
      return;
    }

    $order = $payment->getOrder();

    // See if we have an amount set to track. If so, check it matches the
    // payment amount, otherwise ignore it.
    $tracking = $this->loadTrackingInformation($order);
    if ($tracking && !$this->validateTracking($payment, $tracking)) {
      $tracking = NULL;
    }

    // Otherwise check we are paying in full and, if so, we can construct it.
    if (!$tracking && $payment_amount && $payment_amount->equals($order->getBalance())) {
      $tracking = $this->calculatePaidInFullTracking($order);
    }

    // If we have tracking information, set it on the payment.
    if ($tracking) {
      $payment->set('order_item_tracking', $tracking);
    }
    else {
      $this->logger->error('No valid payment tracking information for creating a payment for order %id', [
        '%id' => $order->id(),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculatePaidInFullTracking(OrderInterface $order, bool $store = FALSE) : array {
    $tracking = [];

    $paid = $this->getTrackedAmountsForOrder($order);
    foreach ($order->getItems() as $item) {
      $item_id = $item->id();

      // Start with the adjusted total price.
      $amount = $item->getAdjustedTotalPrice();

      // Subtract any already paid amounts.
      if (!$paid[$item_id]->isZero()) {
        // Throw an exception if the tracked amount is in a different currency.
        if ($amount->getCurrencyCode() != $paid[$item_id]->getCurrencyCode()) {
          throw new \Exception('Item total and tracked amount must use the same currency.');
        }

        $amount = $amount->subtract($paid[$item_id]);
      }

      // Skip anything that is zero value.
      if ($amount->isZero()) {
        continue;
      }

      // Store this in the tracking array.
      $tracking[$item_id] = $amount->toArray() + ['target_id' => $item_id];
    }

    if ($store) {
      $this->storeTrackingInformation($order, $tracking);
    }

    return $tracking;
  }

  /**
   * {@inheritdoc}
   */
  public function storeTrackingInformation(OrderInterface $order, array $tracking) : void {
    $this->privateTempStore->set($this->getKey($order), $tracking);
  }

  /**
   * {@inheritdoc}
   */
  public function loadTrackingInformation(OrderInterface $order) : ?array {
    return $this->privateTempStore->get($this->getKey($order));
  }

  /**
   * Get the key for the order tracking information.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return string
   *   The storage key.
   */
  protected function getKey(OrderInterface $order) {
    // We key by both order ID and route name to make sure that two separate
    // processes on the same order are kept distinct.
    return $order->id() . '|' . $this->route->getRouteName();
  }

  /**
   * {@inheritdoc}
   */
  public function validateTracking(PaymentInterface $payment, ?array $tracking = NULL) : ?bool {
    // Get tracking from the payment if not provided.
    if (!isset($tracking)) {
      $tracking = $payment->get('order_item_tracking')->getValue();
    }

    // Return NULL on empty tracking to indicate we've not really checked
    // anything.
    if (empty($tracking)) {
      return NULL;
    }

    // Get our total of the tracking.
    /** @var \Drupal\commerce_price\Price|null $total */
    $total = NULL;
    foreach ($tracking as $tracking_item) {
      $item_amount = Price::fromArray($tracking_item);
      $total = $total ? $total->add($item_amount) : $item_amount;
    }

    // Get the payment total.
    $amount = $payment->getAmount();

    // Make sure the payment amount matches the tracking total.
    if ($amount && $total) {
      return $amount->equals($total);
    }
    elseif ($amount) {
      return $amount->isZero();
    }
    elseif ($total) {
      return $total->isZero();
    }
    else {
      return TRUE;
    }
  }

  /**
   * Get an empty price field.
   *
   * @param string|null $currency
   *   The currency to use, or NULL for the store default (if set).
   *
   * @return \Drupal\commerce_price\Price
   *   The empty price.
   *
   * @throws \Exception
   *   Thrown if the payment is not paying the full order balance, but there is
   *   no tracking information.
   */
  protected function getEmptyPrice(?string $currency) {
    // If we don't have a default currency to use, use the default store's
    // default currency.
    if (!isset($currency)) {
      $currency = $this->defaultCurrency;
    }

    // If there isn't one, throw an exception.
    if (!isset($currency)) {
      throw new \Exception("There is no default store configured to fall back to for a default currency.");
    }

    return new Price('0', $currency);
  }

}
