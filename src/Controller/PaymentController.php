<?php

namespace Drupal\commerce_partial_payments\Controller;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce_partial_payments\Form\AddTrackingForm;
use Drupal\commerce_partial_payments\OrderItemTrackingInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Commerce Partial Payments routes.
 */
class PaymentController extends ControllerBase {

  /**
   * The order item tracking service.
   *
   * @var \Drupal\commerce_partial_payments\OrderItemTrackingInterface
   */
  protected $tracker;

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $formatter;

  /**
   * The controller constructor.
   *
   * @param \Drupal\commerce_partial_payments\OrderItemTrackingInterface $tracker
   *   The order item tracking service.
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $formatter
   *   The currency formatter.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(OrderItemTrackingInterface $tracker, CurrencyFormatterInterface $formatter, FormBuilderInterface $form_builder) {
    $this->tracker = $tracker;
    $this->formatter = $formatter;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_partial_payments.order_item_tracking'),
      $container->get('commerce_price.currency_formatter'),
      $container->get('form_builder')
    );
  }

  /**
   * Builds the response.
   */
  public function tracking(PaymentInterface $commerce_payment) {
    $tracking = $commerce_payment->get('order_item_tracking');

    if ($tracking->isEmpty()) {
      return $this->formBuilder->getForm(AddTrackingForm::class, $commerce_payment);
    }

    $build['order_item_tracking'] = [
      '#type' => 'table',
      '#title' => $this->t('Payment tracking'),
      '#theme_wrappers' => ['form_element'],
      '#wrapper_attributes' => [
        'class' => ['cpp-tracking-amounts'],
      ],
      '#header' => [
        'item' => $this->t('Item'),
        'total' => $this->t('Total'),
        'payment' => $this->t('Tracked payment'),
      ],
      '#empty' => $this->t('This payment has no tracking information.'),
      '#states' => [
        'visible' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
        'required' => [
          ':input[name="payment[partial_payments_type]"]' => ['value' => 'partial'],
        ],
      ],
    ];

    /** @var \Drupal\commerce_price\Price|null $total */
    $total = NULL;
    foreach ($tracking as $item) {
      $amount = $item->toPrice();
      $total = $total ? $total->add($amount) : $amount;

      /** @var \Drupal\commerce_partial_payments\Plugin\Field\FieldType\CommerceTrackedAmountItem $item */
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface|null $order_item */
      $order_item = $item->entity;
      if ($order_item && $order_item->getOrderId() == $commerce_payment->getOrderId()) {
        $label = $order_item->label();
        $item_total = $order_item->getAdjustedTotalPrice();
      }
      else {
        $label = $this->t('Unknown item %id', ['%id' => $item->target_id]);
        $item_total = NULL;
      }

      // phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
      $build['order_item_tracking'][$item->target_id] = [
        'item' => ['#markup' => $label],
        'total' => ['#markup' => $item_total ? $this->formatter->format($item_total->getNumber(), $item_total->getCurrencyCode()) : ''],
        'payment' => ['#markup' => $this->formatter->format($amount->getNumber(), $amount->getCurrencyCode())],
      ];
      // phpcs:enable
    }

    // Get the total paid, including refunds.
    $payment_amount = $commerce_payment->getAmount();
    if ($refund_amount = $commerce_payment->getRefundedAmount()) {
      $payment_amount = $payment_amount->subtract($refund_amount);
    }

    $build['total'] = [
      '#type' => 'item',
      '#title' => $this->t('Payment total'),
      '#markup' => $this->formatter->format($payment_amount->getNumber(), $payment_amount->getCurrencyCode()),
    ];

    if (($total xor $payment_amount) || !$payment_amount->equals($total)) {
      $this->messenger()->addWarning("The tracked amount doesn't match the payment total.");
      $build['tracked'] = [
        '#type' => 'item',
        '#title' => $this->t('Tracked total'),
        '#markup' => $this->formatter->format($total->getNumber(), $total->getCurrencyCode()),
      ];
    }

    return $build;
  }

}
