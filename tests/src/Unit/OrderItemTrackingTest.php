<?php

namespace Drupal\Tests\commerce_partial_payments\Unit;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_partial_payments\OrderItemTracking;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentStorageInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\commerce_store\StoreStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryAggregateInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/**
 * Tests the Order Item Tracking service.
 *
 * @group commerce_partial_payments
 *
 * @coversDefaultClass \Drupal\commerce_partial_payments\OrderItemTracking
 */
class OrderItemTrackingTest extends UnitTestCase {

  /**
   * The tempstore key value pairs.
   *
   * @var array
   */
  protected $tempstore = [];

  /**
   * Tests storage and retrieval of tracking information.
   *
   * @param int|null $set_order_id
   *   The order ID for storing values, or NULL to not set.
   * @param string|null $set_route_name
   *   The route name when storing values, or NULL if $set_order_id is NULL.
   * @param array|null $tracking
   *   The tracking values to set, or NULL if $set_order_id is NULL.
   * @param int $get_order_id
   *   The order ID for loading values.
   * @param string $get_route_name
   *   The route name when loading values.
   * @param array|null $expected
   *   The tracking values we expect to be returned.
   *
   * @covers ::storeTrackingInformation
   * @covers ::loadTrackingInformation
   *
   * @dataProvider dataStorage
   */
  public function testStorage(?int $set_order_id, ?string $set_route_name, ?array $tracking, int $get_order_id, string $get_route_name, ?array $expected) {
    // Set values, if required.
    if ($set_order_id) {
      $service = $this->getService(NULL, NULL, $set_route_name);
      $order = $this->prophesize(OrderInterface::class);
      $order->id()->willReturn($set_order_id);
      $service->storeTrackingInformation($order->reveal(), $tracking);
    }

    // Retrieve the value and check it.
    $service = $this->getService(NULL, NULL, $get_route_name);
    $order = $this->prophesize(OrderInterface::class);
    $order->id()->willReturn($get_order_id);
    $this->assertSame($expected, $service->loadTrackingInformation($order->reveal()));
  }

  /**
   * Data provider for ::testStorage.
   */
  public static function dataStorage() {
    yield 'set-and-get' => [
      'set_order_id' => 10,
      'set_route_name' => 'commerce_checkout',
      'tracking' => [
        [
          'target_id' => 50,
          'number' => '20.00',
          'currency_code' => 'GBP',
        ],
      ],
      'get_order_id' => 10,
      'get_route_name' => 'commerce_checkout',
      'expected_tracking' => [
        [
          'target_id' => 50,
          'number' => '20.00',
          'currency_code' => 'GBP',
        ],
      ],
    ];

    yield 'not-set' => [
      'set_order_id' => NULL,
      'set_route_name' => NULL,
      'tracking' => NULL,
      'get_order_id' => 10,
      'get_route_name' => 'commerce_checkout',
      'expected_tracking' => NULL,
    ];

    yield 'different-order' => [
      'set_order_id' => 5,
      'set_route_name' => 'commerce_checkout',
      'tracking' => [
        [
          'target_id' => 20,
          'number' => '50.00',
          'currency_code' => 'GBP',
        ],
      ],
      'get_order_id' => 10,
      'get_route_name' => 'commerce_checkout',
      'expected_tracking' => NULL,
    ];

    yield 'different-route' => [
      'set_order_id' => 10,
      'set_route_name' => 'payment_admin',
      'tracking' => [
        [
          'target_id' => 20,
          'number' => '50.00',
          'currency_code' => 'GBP',
        ],
      ],
      'get_order_id' => 10,
      'get_route_name' => 'commerce_checkout',
      'expected_tracking' => NULL,
    ];
  }

  /**
   * Test the calculation for paying in full.
   *
   * @param array $total_price
   *   The total price of the order.
   * @param array $items
   *   The items on the order.
   * @param array $expected
   *   The expected tracking information.
   * @param array $existing_tracking
   *   The results of the tracking query.
   *
   * @covers ::calculatePaidInFullTracking
   *
   * @dataProvider dataCalculate
   */
  public function testCalculate(array $total_price, array $items, array $expected, array $existing_tracking = []) {
    $order = $this->prophesize(OrderInterface::class);

    $order->id()->willReturn(1);
    $order->getTotalPrice()->willReturn(new Price(...$total_price));

    $order_items_values = [];
    $order_items = [];
    foreach ($items as $item) {
      $order_items_values[] = ['target_id' => $item['id']];

      $order_item_entity = $this->prophesize(OrderItemInterface::class);
      $order_item_entity->id()->willReturn($item['id']);
      $order_item_entity->getAdjustedTotalPrice()->willReturn(new Price(...$item['price']));
      $order_items[] = $order_item_entity->reveal();
    }
    $order_items_items = $this->prophesize(EntityReferenceFieldItemListInterface::class);
    $order_items_items->getValue()->willReturn($order_items_values);
    $order->get('order_items')->willReturn($order_items_items->reveal());
    $order->getItems()->willReturn($order_items);

    $service = $this->getService(NULL, $existing_tracking);
    $result = $service->calculatePaidInFullTracking($order->reveal(), FALSE);
    $this->assertSame($expected, $result);
  }

  /**
   * Data provider for ::testCalculate.
   */
  public static function dataCalculate() {
    yield 'no-existing' => [
      'total_price' => ['70.00', 'GBP'],
      'items' => [
        [
          'id' => 11,
          'price' => ['20.00', 'GBP'],
        ],
        [
          'id' => 12,
          'price' => ['50.00', 'GBP'],
        ],
      ],
      'expected' => [
        11 => [
          'number' => '20.00',
          'currency_code' => 'GBP',
          'target_id' => 11,
        ],
        12 => [
          'number' => '50.00',
          'currency_code' => 'GBP',
          'target_id' => 12,
        ],
      ],
    ];

    yield 'existing-some-partial' => [
      'total_price' => ['70.00', 'GBP'],
      'items' => [
        [
          'id' => 11,
          'price' => ['20.00', 'GBP'],
        ],
        [
          'id' => 12,
          'price' => ['50.00', 'GBP'],
        ],
      ],
      'expected' => [
        11 => [
          'number' => '20.00',
          'currency_code' => 'GBP',
          'target_id' => 11,
        ],
        12 => [
          'number' => '20',
          'currency_code' => 'GBP',
          'target_id' => 12,
        ],
      ],
      'existing_tracking' => [
        [
          'order_item_tracking_target_id' => 12,
          'order_item_trackingnumber_sum' => '30.00',
          'order_item_tracking_currency_code' => 'GBP',
        ],
      ],
    ];

    yield 'existing-all-partial' => [
      'total_price' => ['70.00', 'GBP'],
      'items' => [
        [
          'id' => 11,
          'price' => ['20.00', 'GBP'],
        ],
        [
          'id' => 12,
          'price' => ['50.00', 'GBP'],
        ],
      ],
      'expected' => [
        11 => [
          'number' => '10',
          'currency_code' => 'GBP',
          'target_id' => 11,
        ],
        12 => [
          'number' => '20',
          'currency_code' => 'GBP',
          'target_id' => 12,
        ],
      ],
      'existing_tracking' => [
        [
          'order_item_tracking_target_id' => 12,
          'order_item_trackingnumber_sum' => '30.00',
          'order_item_tracking_currency_code' => 'GBP',
        ],
        [
          'order_item_tracking_target_id' => 11,
          'order_item_trackingnumber_sum' => '10.00',
          'order_item_tracking_currency_code' => 'GBP',
        ],
      ],
    ];

    yield 'existing-some-full' => [
      'total_price' => ['70.00', 'GBP'],
      'items' => [
        [
          'id' => 11,
          'price' => ['20.00', 'GBP'],
        ],
        [
          'id' => 12,
          'price' => ['50.00', 'GBP'],
        ],
      ],
      'expected' => [
        11 => [
          'number' => '20.00',
          'currency_code' => 'GBP',
          'target_id' => 11,
        ],
      ],
      'existing_tracking' => [
        [
          'order_item_tracking_target_id' => 12,
          'order_item_trackingnumber_sum' => '50.00',
          'order_item_tracking_currency_code' => 'GBP',
        ],
      ],
    ];

    yield 'existing-all-full' => [
      'total_price' => ['70.00', 'GBP'],
      'items' => [
        [
          'id' => 11,
          'price' => ['20.00', 'GBP'],
        ],
        [
          'id' => 12,
          'price' => ['50.00', 'GBP'],
        ],
      ],
      'expected' => [],
      'existing_tracking' => [
        [
          'order_item_tracking_target_id' => 12,
          'order_item_trackingnumber_sum' => '50.00',
          'order_item_tracking_currency_code' => 'GBP',
        ],
        [
          'order_item_tracking_target_id' => 11,
          'order_item_trackingnumber_sum' => '20.00',
          'order_item_tracking_currency_code' => 'GBP',
        ],
      ],
    ];

    yield 'existing-mix-partial-full' => [
      'total_price' => ['70.00', 'GBP'],
      'items' => [
        [
          'id' => 11,
          'price' => ['20.00', 'GBP'],
        ],
        [
          'id' => 12,
          'price' => ['50.00', 'GBP'],
        ],
      ],
      'expected' => [
        11 => [
          'number' => '10',
          'currency_code' => 'GBP',
          'target_id' => 11,
        ],
      ],
      'existing_tracking' => [
        [
          'order_item_tracking_target_id' => 12,
          'order_item_trackingnumber_sum' => '50.00',
          'order_item_tracking_currency_code' => 'GBP',
        ],
        [
          'order_item_tracking_target_id' => 11,
          'order_item_trackingnumber_sum' => '10.00',
          'order_item_tracking_currency_code' => 'GBP',
        ],
      ],
    ];
  }

  /**
   * Tests the tracked amount constraint and validator.
   *
   * @param string $amount
   *   The payment amount.
   * @param array|null $tracking
   *   The payment order item tracking field value.
   * @param bool|null $expected
   *   The expected result.
   *
   * @covers ::validateTracking
   *
   * @dataProvider dataValidate
   */
  public function testValidate(string $amount, ?array $tracking, ?bool $expected) {
    $payment = $this->prophesize(PaymentInterface::class);

    // Configure payment amount retrieval.
    $payment->getAmount()->willReturn(new Price($amount, 'GBP'));

    // Configure tracking retrieval.
    $tracking_field_list = $this->prophesize(EntityReferenceFieldItemListInterface::class);
    $tracking_field_list->getValue()->willReturn($tracking);
    $payment->get('order_item_tracking')->willReturn($tracking_field_list->reveal());

    $service = $this->getService();

    $result = $service->validateTracking($payment->reveal());
    $this->assertSame($expected, $result);
  }

  /**
   * Data provider for ::testValidate.
   */
  public static function dataValidate() {
    yield 'no-tracking' => [
      'amount' => '10',
      'tracking' => NULL,
      'expected' => NULL,
    ];

    yield 'valid-single-tracking' => [
      'amount' => '10',
      'tracking' => [
        [
          'target_id' => 10,
          'number' => '10',
          'currency_code' => 'GBP',
        ],
      ],
      'expected' => TRUE,
    ];

    yield 'valid-split-tracking' => [
      'amount' => '10',
      'tracking' => [
        [
          'target_id' => 10,
          'number' => '6',
          'currency_code' => 'GBP',
        ],
        [
          'target_id' => 11,
          'number' => '4',
          'currency_code' => 'GBP',
        ],
      ],
      'expected' => TRUE,
    ];

    yield 'valid-zero-amount' => [
      'amount' => '0',
      'tracking' => [
        [
          'target_id' => 10,
          'number' => '6',
          'currency_code' => 'GBP',
        ],
        [
          'target_id' => 11,
          'number' => '-6',
          'currency_code' => 'GBP',
        ],
      ],
      'expected' => TRUE,
    ];

    yield 'invalid-single-tracking' => [
      'amount' => '10',
      'tracking' => [
        [
          'target_id' => 10,
          'number' => '6',
          'currency_code' => 'GBP',
        ],
      ],
      'expected' => FALSE,
    ];

    yield 'invalid-split-tracking' => [
      'amount' => '10',
      'tracking' => [
        [
          'target_id' => 10,
          'number' => '5',
          'currency_code' => 'GBP',
        ],
        [
          'target_id' => 11,
          'number' => '4',
          'currency_code' => 'GBP',
        ],
      ],
      'expected' => FALSE,
    ];

    yield 'invalid-zero-amount' => [
      'amount' => '0',
      'tracking' => [
        [
          'target_id' => 10,
          'number' => '6',
          'currency_code' => 'GBP',
        ],
        [
          'target_id' => 11,
          'number' => '-5',
          'currency_code' => 'GBP',
        ],
      ],
      'expected' => FALSE,
    ];
  }

  /**
   * Get the service, building the arguments.
   *
   * @param string|null $default_currency
   *   The default currency to use, or NULL if there is no default store.
   * @param array|null $query_results
   *   The query results, or NULL if there is no query.
   * @param string $route_name
   *   The current route name.
   *
   * @return \Drupal\commerce_partial_payments\OrderItemTracking
   *   The order item tracking service.
   */
  protected function getService(?string $default_currency = NULL, ?array $query_results = NULL, $route_name = 'commerce_checkout') {
    $tempstore = $this->prophesize(PrivateTempStore::class);
    $tempstore_values = &$this->tempstore;
    $tempstore->set(Argument::type('string'), Argument::any())
      ->will(function ($args) use (&$tempstore_values) {
        $tempstore_values[$args[0]] = $args[1];
      });
    $tempstore->get(Argument::type('string'))
      ->will(function ($args) use (&$tempstore_values) {
        return $tempstore_values[$args[0]] ?? NULL;
      });

    $tempstore_factory = $this->prophesize(PrivateTempStoreFactory::class);
    $tempstore_factory->get('commerce_partial_payments.order_item_tracking')
      ->willReturn($tempstore->reveal());

    $route_match = $this->prophesize(CurrentRouteMatch::class);
    $route_match->getRouteName()->willReturn($route_name);
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);

    // Set up the store storage.
    $store = NULL;
    if ($default_currency) {
      $store_mock = $this->prophesize(StoreInterface::class);
      $store_mock->getDefaultCurrencyCode()
        ->willReturn($default_currency);
      $store = $store_mock->reveal();
    }

    $store_storage = $this->prophesize(StoreStorageInterface::class);
    $store_storage->loadDefault()
      ->willReturn($store);

    $entity_type_manager->getStorage('commerce_store')
      ->willReturn($store_storage->reveal());

    // Set up the payment storage.
    $payment_storage = $this->prophesize(PaymentStorageInterface::class);

    if (isset($query_results)) {
      $query = $this->prophesize(QueryAggregateInterface::class);
      $query->exists(Argument::any())
        ->will(function () {
          return $this;
        });
      $query->aggregate(Argument::any(), Argument::any())
        ->will(function () {
          return $this;
        });
      $query->condition(Argument::any(), Argument::any(), Argument::any())
        ->will(function () {
          return $this;
        });
      $query->groupBy(Argument::any())
        ->will(function () {
          return $this;
        });
      $query->accessCheck(Argument::any())
        ->will(function () {
          return $this;
        });
      $query->execute()->willReturn($query_results);
      $payment_storage->getAggregateQuery()->willReturn($query->reveal());
    }

    $entity_type_manager->getStorage('commerce_payment')
      ->willReturn($payment_storage->reveal());

    $logger = $this->prophesize(LoggerInterface::class);

    return new OrderItemTracking($tempstore_factory->reveal(), $route_match->reveal(), $entity_type_manager->reveal(), $logger->reveal());
  }

}
