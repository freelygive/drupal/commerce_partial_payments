<?php

namespace Drupal\Tests\commerce_partial_payments\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the Order Item Tracking service.
 *
 * @group commerce_partial_payments
 *
 * @coversDefaultClass \Drupal\commerce_partial_payments\OrderItemTracking
 */
class OrderItemTrackingTest extends CommerceKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_order',
    'commerce_partial_payments',
    'commerce_payment',
    'entity_reference_revisions',
    'profile',
    'state_machine',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    'commerce_number_pattern.commerce_number_pattern.order_default',
  ];

  /**
   * The order updater, if it exists.
   *
   * @var \Drupal\commerce_payment\PaymentOrderUpdaterInterface|null
   */
  protected $orderUpdater;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_payment');
    $this->installConfig(['commerce_order', 'commerce_payment']);

    // A simple order item.
    OrderItemType::create([
      'id' => 'test',
      'label' => 'Test',
      'orderType' => 'default',
    ])->save();

    // A simple payment gateway.
    PaymentGateway::create([
      'id' => 'test',
      'label' => 'Test',
      'plugin' => 'manual',
    ])->save();

    // Commerce 2.14 added a deferred order updater for payment changes. To
    // ensure the order is up to date, we will manually clear it. Prior to 2.14
    // this service didn't exist, so this will return NULL.
    $this->orderUpdater = $this->container->get('commerce_payment.order_updater', ContainerInterface::NULL_ON_INVALID_REFERENCE);
  }

  /**
   * Tests retrieving the tracked amounts for an order.
   *
   * @covers ::getTrackedAmountsForOrder
   */
  public function testOrderTrackedAmounts() {
    $lookups = [];

    foreach ($this->dataOrderTrackedAmounts() as $label => $data) {
      /** @var array $items */
      /** @var array $payments */
      /** @var array $expected */
      extract($data);

      $order_items = [];
      foreach ($items as $values) {
        $order_item = OrderItem::create($values + [
          'type' => 'test',
          'quantity' => 1,
        ]);
        $order_item->save();
        $order_items[] = $order_item;
      }

      $order = Order::create([
        'type' => 'default',
        'store_id' => $this->store,
        'order_items' => $order_items,
        'payment_gateway' => 'test',
      ]);
      $order->save();

      foreach ($payments as $values) {
        $payment = Payment::create($values + [
          'order_id' => $order->id(),
          'type' => 'payment_default',
          'payment_gateway' => 'test',
          'state' => 'completed',
        ]);
        $payment->save();

        if ($this->orderUpdater) {
          $this->orderUpdater->updateOrders();
        }
      }

      $lookups[] = compact('label', 'order', 'expected');
    }

    $service = $this->container->get('commerce_partial_payments.order_item_tracking');

    foreach ($lookups as $data) {
      /** @var string $label */
      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      /** @var array $expected */
      extract($data);

      $result = $service->getTrackedAmountsForOrder($order);
      foreach ($result as &$amount) {
        $amount = $amount->toArray();
      }
      $this->assertSame($expected, $result, $label);
    }
  }

  /**
   * Data provider for ::testOrderTrackedAmounts.
   */
  protected function dataOrderTrackedAmounts() {
    yield 'single_full_payment' => [
      'items' => [
        [
          'order_item_id' => 10,
          'unit_price' => ['number' => '10', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '10', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 10,
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
      'expected' => [
        10 => ['number' => '10.000000', 'currency_code' => 'USD'],
      ],
    ];

    yield 'single_partial_payment' => [
      'items' => [
        [
          'order_item_id' => 11,
          'unit_price' => ['number' => '10', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '5', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 11,
              'number' => '5',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
      'expected' => [
        11 => ['number' => '5.000000', 'currency_code' => 'USD'],
      ],
    ];

    yield 'multiple_payments' => [
      'items' => [
        [
          'order_item_id' => 12,
          'unit_price' => ['number' => '10', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '6', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 12,
              'number' => '6',
              'currency_code' => 'USD',
            ],
          ],
        ],
        [
          'amount' => ['number' => '4', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 12,
              'number' => '4',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
      'expected' => [
        12 => ['number' => '10.000000', 'currency_code' => 'USD'],
      ],
    ];

    yield 'multiple_items' => [
      'items' => [
        [
          'order_item_id' => 13,
          'unit_price' => ['number' => '10', 'currency_code' => 'USD'],
        ],
        [
          'order_item_id' => 14,
          'unit_price' => ['number' => '10', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 13,
              'number' => '10',
              'currency_code' => 'USD',
            ],
            [
              'target_id' => 14,
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
      'expected' => [
        13 => ['number' => '10.000000', 'currency_code' => 'USD'],
        14 => ['number' => '10.000000', 'currency_code' => 'USD'],
      ],
    ];

    yield 'removed_items' => [
      'items' => [
        [
          'order_item_id' => 16,
          'unit_price' => ['number' => '10', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 15,
              'number' => '10',
              'currency_code' => 'USD',
            ],
            [
              'target_id' => 16,
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
      'expected' => [
        15 => ['number' => '10.000000', 'currency_code' => 'USD'],
        16 => ['number' => '10.000000', 'currency_code' => 'USD'],
      ],
    ];

    yield 'unpaid_items' => [
      'items' => [
        [
          'order_item_id' => 17,
          'unit_price' => ['number' => '10', 'currency_code' => 'USD'],
        ],
        [
          'order_item_id' => 18,
          'unit_price' => ['number' => '10', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '10', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 17,
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
      'expected' => [
        17 => ['number' => '10.000000', 'currency_code' => 'USD'],
        18 => ['number' => '0', 'currency_code' => 'USD'],
      ],
    ];

    yield 'incomplete_payments' => [
      'items' => [
        [
          'order_item_id' => 19,
          'unit_price' => ['number' => '15', 'currency_code' => 'USD'],
        ],
        [
          'order_item_id' => 20,
          'unit_price' => ['number' => '25', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 19,
              'number' => '10',
              'currency_code' => 'USD',
            ],
            [
              'target_id' => 20,
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
        [
          'state' => 'pending',
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 19,
              'number' => '5',
              'currency_code' => 'USD',
            ],
            [
              'target_id' => 20,
              'number' => '15',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
      'expected' => [
        19 => ['number' => '10.000000', 'currency_code' => 'USD'],
        20 => ['number' => '10.000000', 'currency_code' => 'USD'],
      ],
    ];

    yield 'untracked_payment' => [
      'items' => [
        [
          'order_item_id' => 21,
          'unit_price' => ['number' => '15', 'currency_code' => 'USD'],
        ],
        [
          'order_item_id' => 22,
          'unit_price' => ['number' => '25', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 21,
              'number' => '10',
              'currency_code' => 'USD',
            ],
            [
              'target_id' => 22,
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
        [
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
        ],
      ],
      'expected' => [
        21 => ['number' => '15.000000', 'currency_code' => 'USD'],
        22 => ['number' => '25.000000', 'currency_code' => 'USD'],
      ],
    ];

    yield 'partially_refunded_payment' => [
      'items' => [
        [
          'order_item_id' => 23,
          'unit_price' => ['number' => '15', 'currency_code' => 'USD'],
        ],
        [
          'order_item_id' => 24,
          'unit_price' => ['number' => '25', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 23,
              'number' => '10',
              'currency_code' => 'USD',
            ],
            [
              'target_id' => 24,
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
        [
          'state' => 'partially_refunded',
          'completed' => time(),
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
          'refunded_amount' => ['number' => '10', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 23,
              'number' => '5',
              'currency_code' => 'USD',
            ],
            [
              'target_id' => 24,
              'number' => '5',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
      'expected' => [
        23 => ['number' => '15.000000', 'currency_code' => 'USD'],
        24 => ['number' => '15.000000', 'currency_code' => 'USD'],
      ],
    ];

    yield 'refunded_payment' => [
      'items' => [
        [
          'order_item_id' => 25,
          'unit_price' => ['number' => '15', 'currency_code' => 'USD'],
        ],
        [
          'order_item_id' => 26,
          'unit_price' => ['number' => '25', 'currency_code' => 'USD'],
        ],
      ],
      'payments' => [
        [
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 25,
              'number' => '10',
              'currency_code' => 'USD',
            ],
            [
              'target_id' => 26,
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
        [
          'state' => 'refunded',
          'completed' => time(),
          'amount' => ['number' => '20', 'currency_code' => 'USD'],
          'refunded_amount' => ['number' => '20', 'currency_code' => 'USD'],
          'order_item_tracking' => [
            [
              'target_id' => 25,
              'number' => '10',
              'currency_code' => 'USD',
            ],
            [
              'target_id' => 26,
              'number' => '10',
              'currency_code' => 'USD',
            ],
          ],
        ],
      ],
      'expected' => [
        25 => ['number' => '10.000000', 'currency_code' => 'USD'],
        26 => ['number' => '10.000000', 'currency_code' => 'USD'],
      ],
    ];
  }

}
