<?php

/**
 * @file
 * Views hooks for Commerce Partial Payments.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data().
 */
function commerce_partial_payments_views_data() {
  $data['commerce_order_item']['paid'] = [
    'title' => new TranslatableMarkup('Paid'),
    'help' => new TranslatableMarkup('The amount paid towards this item.'),
    'field' => [
      'field_name' => 'paid',
      'id' => 'field',
    ],
  ];
  $data['commerce_order_item']['balance'] = [
    'title' => new TranslatableMarkup('Balance'),
    'help' => new TranslatableMarkup('The balance for this item.'),
    'field' => [
      'field_name' => 'balance',
      'id' => 'field',
    ],
  ];
  return $data;
}
