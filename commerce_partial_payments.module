<?php

/**
 * @file
 * Primary module hooks for Commerce Partial Payments module.
 */

use Drupal\commerce_partial_payments\Plugin\Commerce\InlineForm\PartialPaymentsGatewayForm;
use Drupal\commerce_partial_payments\Plugin\Field\ComputedOrderItemTracking;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Implements hook_entity_base_field_info().
 */
function commerce_partial_payments_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];

  if ($entity_type->id() == 'commerce_payment') {
    $fields['order_item_tracking'] = BaseFieldDefinition::create('commerce_tracked_amount')
      ->setName('order_item_tracking')
      ->setTargetEntityTypeId('commerce_payment')
      ->setLabel(new TranslatableMarkup('Order item tracking'))
      ->setDescription(new TranslatableMarkup('Tracks the distribution of the payment against the order items.'))
      ->setSetting('target_type', 'commerce_order_item')
      ->setSetting('handler', 'default')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);
  }

  if ($entity_type->id() == 'commerce_order_item') {
    $fields['paid'] = BaseFieldDefinition::create('commerce_price')
      ->setName('paid')
      ->setTargetEntityTypeId('commerce_order_item')
      ->setLabel(new TranslatableMarkup('Paid'))
      ->setDescription(new TranslatableMarkup('The amount paid towards this item.'))
      ->setComputed(TRUE)
      ->setReadOnly(TRUE)
      ->setClass(ComputedOrderItemTracking::class)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['balance'] = BaseFieldDefinition::create('commerce_price')
      ->setName('balance')
      ->setTargetEntityTypeId('commerce_order_item')
      ->setLabel(new TranslatableMarkup('Balance'))
      ->setDescription(new TranslatableMarkup('The balance for this item.'))
      ->setComputed(TRUE)
      ->setReadOnly(TRUE)
      ->setSetting('tracking_balance', TRUE)
      ->setClass(ComputedOrderItemTracking::class)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);
  }

  return $fields;
}

/**
 * Implements hook_ENTITY_TYPE_create().
 */
function commerce_partial_payments_commerce_payment_create(EntityInterface $entity) {
  \Drupal::service('commerce_partial_payments.order_item_tracking')->onPaymentCreate($entity);
}

/**
 * Implements hook_inline_entity_form_table_fields_alter().
 */
function commerce_partial_payments_inline_entity_form_table_fields_alter(&$fields, $context) {
  if ($context['parent_entity_type'] == 'commerce_order' && $context['entity_type'] == 'commerce_order_item') {
    // Don't show the columns if we know the user wont have access. We have to
    // check permissions directly as we don't have the actual object to act on
    // at this stage. This means entity access hooks or other special cases wont
    // affect whether we show. Administer payments will always grant access.
    $permissions = [
      'administer commerce_payment',
    ];

    // Next add in permissions relating to our config.
    $tracking_access = \Drupal::config('commerce_partial_payments.settings')
      ->get('tracking_access');

    // If we have any config, allow the administer order permission.
    if (!empty($tracking_access)) {
      $permissions[] = 'administer commerce_order';

      // Add the operation, own operation and bundle operation. They may not all
      // exist, but if they don't they'll be ignored.
      foreach ($tracking_access as $operation) {
        $permissions[] = "{$operation} commerce_order";
        $permissions[] = "{$operation} own commerce_order";
        $permissions[] = "{$operation} {$context['parent_bundle']} commerce_order";
      }
    }

    // Now check if the user has any of these permissions.
    $user = \Drupal::currentUser();
    $has_any = FALSE;
    foreach ($permissions as $permission) {
      if ($user->hasPermission($permission)) {
        $has_any = TRUE;
        break;
      }
    }

    // If they don't have any, we wont add the fields.
    if (!$has_any) {
      return;
    }

    $fields['paid'] = [
      'type' => 'field',
      'label' => new TranslatableMarkup('Paid'),
      'weight' => 2.1,
    ];
    $fields['balance'] = [
      'type' => 'field',
      'label' => new TranslatableMarkup('Balance'),
      'weight' => 2.2,
    ];
  }
}

/**
 * Implements hook_entity_operation().
 */
function commerce_partial_payments_entity_operation(EntityInterface $entity) {
  $operations = [];

  if ($entity->getEntityTypeId() == 'commerce_payment') {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $entity */
    $operations['cpp_tracking'] = [
      'title' => t('Tracking'),
      'url' => Url::fromRoute('commerce_partial_payments.payment.tracking', [
        'commerce_order' => $entity->getOrderId(),
        'commerce_payment' => $entity->id(),
      ]),
      'weight' => 50,
    ];
  }

  return $operations;
}

/**
 * Implements hook_entity_operation_alter().
 */
function commerce_partial_payments_entity_operation_alter(array &$operations, EntityInterface $entity) {
  if ($entity->getEntityTypeId() == 'commerce_payment') {
    /** @var \Drupal\commerce_payment\Entity\Payment $entity */
    if ($entity->getPaymentGatewayId() == 'reallocate_funds') {
      // Don't allow the Refund operation on fund reallocation (as there's
      // nothing to actually refund).
      unset($operations['refund']);
    }
  }
}

/**
 * Implements hook_commerce_inline_form_info_alter().
 */
function commerce_partial_payments_commerce_inline_form_info_alter(&$info) {
  $info['payment_gateway_form']['class'] = PartialPaymentsGatewayForm::class;
}
